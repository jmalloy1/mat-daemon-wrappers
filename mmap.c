#define _GNU_SOURCE
#define HMALLOC_MSG_SENDER
#define HMALLOC_MSG_IMPL
#include <dlfcn.h>
#include <sys/types.h>
#include "hmalloc_msg.h"
#include <time.h>
#include <sys/mman.h>
#include <unistd.h>

#include <stdlib.h>

pid_t pid;
hmalloc_msg_sender sender;
size_t setup = 0;

static void* (*real_mmap)(void *addr, size_t length, int prot, int flags,int fd, off_t offset)= NULL;
static int (*real_munmap)(void *addr, size_t length) = NULL;

void *mmap(void *addr, size_t length, int prot, int flags, int fd, off_t offset){
    if(!setup){
        real_mmap = dlsym(RTLD_NEXT,"mmap");
        real_munmap = dlsym(RTLD_NEXT,"munmap");
        setup=1;
        hmsg_start_sender(&sender);
        pid = getpid();
        hmalloc_msg msg;
        msg.header.msg_type=HMSG_INIT;
        msg.header.pid=pid;
        msg.init.mode=HMSG_MODE_OBJECT;
        hmsg_send(&sender, &msg);
        setup=2;
    }
    void * address = real_mmap(addr,length,prot,flags,fd,offset);
    if(address==NULL||address==MAP_FAILED/*||flags&MAP_SHARED*/){
        return address;
    }
/*     fprintf(stdout,"%p - %zd\n",addr,length); */
    hmalloc_msg msg;
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    msg.header.msg_type=HMSG_ALLO;
    msg.header.pid = pid;
    msg.allo.addr = (uint64_t)address;
    msg.allo.size = length;
    msg.allo.timestamp_ns =1000000000ULL * ts.tv_sec + ts.tv_nsec;
    msg.allo.hid=fd==-1?0:1;
    while(setup==1){

    }
    hmsg_send(&sender,&msg);
    //usleep(100000);
    return address;
}

int munmap(void *addr, size_t length)
{
    int returnValue = real_munmap(addr,length);
    if(returnValue!=0){
        return returnValue;
    }
    hmalloc_msg msg;
    msg.header.msg_type = HMSG_FREE;
    msg.header.pid = pid;
    msg.free.addr = (uint64_t)addr;
    hmsg_send(&sender, &msg);
    return returnValue;
}
