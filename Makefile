CC=		gcc
CCFLAGS=	-Wall -lrt -ldl -fPIC -shared
SHELL=		bash

all:		mmap.so

mmap.so:	mmap.c
		$(CC) $(CCFLAGS) -o $@ $^
init.so:	init.c
		$(CC) $(CCFLAGS) -o $@ $^

clean:
		rm -f *.so
